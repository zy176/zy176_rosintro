#!/usr/bin/bash

# turtle1 drawing letter Z
rosservice call turtle1/set_pen  0 255 0 2 on
rosservice call turtle1/teleport_absolute 2 6 0
rosservice call turtle1/set_pen 0 255 0 2 off
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \ '[4.0,0.0,0.0]' '[0.0,0.0,0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \ '[-4.0,-4.0,0.0]' '[0.0,0.0,0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \ '[4.0,0.0,0.0]' '[0.0,0.0,0.0]'

# turtle2 drawing letter I
rosservice call spawn 8 2 1.57 turtle2
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \ '[3.4,0.0,0.0]' '[0.0,0.0,0.0]'
rosservice call turtle2/set_pen 255 0 0 2 on
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \ '[0.5,0.0,0.0]' '[0.0,0.0,0.0]'
rosservice call turtle2/set_pen 255 0 0 4 off
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \ '[1,0.0,0.0]' '[0.0,0.0,0.0]'
