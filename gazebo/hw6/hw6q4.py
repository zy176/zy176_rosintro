import sys
import rospy
import copy

import moveit_commander
import moveit_msgs.msg
import numpy as np
from geometry_msgs.msg import Pose

robot = moveit_commander.RobotCommander()
group = moveit_commander.MoveGroupCommander("manipulator")

moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('hw6', anonymous=True)
group = moveit_commander.MoveGroupCommander("manipulator")

#assign joint angles
joint_goal = group.get_current_joint_values()
joint_goal[0] = 0
joint_goal[1] = -np.pi / 4
joint_goal[2] = 0
joint_goal[3] = -np.pi / 2
joint_goal[4] = 0
# 30 degree trun
joint_goal[5] = np.pi / 3  

def gotopose_init():
	pose_target = Pose()
	pose_target.position.x = 0.4
	pose_target.position.y = 0.4
	pose_target.position.z = 0.8
	group.set_pose_target(pose_target)
	success = group.go(wait=True)
	group.stop()
	group.clear_pose_targets()
	
def gotopose(x, y, z):
	pose_target = Pose()
	pose_target.position.x = x
	pose_target.position.y = y
	pose_target.position.z = z
	group.set_pose_target(pose_target)
	success = group.go(wait=True)
	group.stop()
	group.clear_pose_targets()

#go to initial position
gotopose_init()


#cartesian path
display_trajectory = moveit_msgs.msg.DisplayTrajectory()
display_trajectory.trajectory_start = robot.get_current_state()
#create waypoints list
path = []
curpos = group.get_current_pose().pose

curpos.position.y -= 0.1 
curpos.position.x += 0.1 
path.append(copy.deepcopy(curpos))

curpos.position.y += 0.2
curpos.position.x -= 0.2
temp = curpos
path.append(copy.deepcopy(curpos))

curpos.position.y -= 0.2
curpos.position.x += 0.2
curpos.position.z -= 0.2
path.append(copy.deepcopy(curpos))

curpos.position.y += 0.2
curpos.position.x -= 0.2
path.append(copy.deepcopy(curpos))

(plan, fraction) = group.compute_cartesian_path(path, 0.01, 0.0) 

display_trajectory.trajectory.append(plan)
group.execute(plan, wait=True)

#draw letter Z
gotopose(x=0.5,y=0.3,z=0.8)
gotopose(x=0.3,y=0.5,z=0.8)
gotopose(x=0.5,y=0.3,z=0.6)
gotopose(x=0.3,y=0.5,z=0.6)




