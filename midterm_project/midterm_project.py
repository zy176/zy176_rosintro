import sys
import rospy
import moveit_commander
import numpy as np
from geometry_msgs.msg import Pose
import geometry_msgs.msg
import time 
import tf.transformations  # Import statement for tf
from tf.transformations import quaternion_matrix
import moveit_commander

moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('hw6', anonymous=True)
robot = moveit_commander.RobotCommander()
group = moveit_commander.MoveGroupCommander("manipulator")

#assign joint angles
joint_goal = group.get_current_joint_values()
joint_goal[0] = 0
joint_goal[1] = -np.pi / 4
joint_goal[2] = 0
joint_goal[3] = -np.pi / 2
joint_goal[4] = 0
# 30 degree trun
joint_goal[5] = np.pi / 3  
pose_target = Pose()
# Quaternion for 180-degree rotation around X-axis

orientation = geometry_msgs.msg.Quaternion(*tf.transformations.quaternion_from_euler(np.pi, 0, 0))



# function to make the robot go to initial position
def gotopose_init():
	pose_target.position.x = 0.3
	pose_target.position.y = 0.3
	pose_target.position.z = 0.8
	pose_target.orientation = orientation

	group.set_pose_target(pose_target)
	success = group.go(wait=True)
	group.stop()
	group.clear_pose_targets()

# function to make robot go to specific position
def gotopose(x, y, z):
	pose_target.position.x = x
	pose_target.position.y = y
	pose_target.position.z = z
	# pose_target.orientation = orientation
	group.set_pose_target(pose_target)
	success = group.go(wait=True)
	group.stop()
	group.clear_pose_targets()

print('go to initial position')
gotopose_init()
current_pose = group.get_current_pose()
rospy.loginfo("Current Pose for initial position: \n" + str(current_pose))
time.sleep(1)
print('draw letter Z')
gotopose(x=0.2,y=0.4,z=0.7)
gotopose(x=0.4,y=0.4,z=0.7)
gotopose(x=0.2,y=0.2,z=0.7)
gotopose(x=0.4,y=0.2,z=0.7)
current_pose = group.get_current_pose()
rospy.loginfo("Current Pose for end of X: \n" + str(current_pose))
time.sleep(1)

print('go to initial position')
gotopose_init()
time.sleep(1)
print('draw letter Y')
gotopose(x=0.2,y=0.4,z=0.7)
gotopose(x=0.3,y=0.3,z=0.7)
gotopose(x=0.4,y=0.4,z=0.7)
gotopose(x=0.3,y=0.3,z=0.7)
gotopose(x=0.3,y=0.2,z=0.7)
current_pose = group.get_current_pose()
rospy.loginfo("Current Pose for end of Y: \n" + str(current_pose))

print('go to initial position')
gotopose_init()
time.sleep(1)
print('draw letter T')
gotopose(x=0.2,y=0.4,z=0.7)
gotopose(x=0.4,y=0.4,z=0.7)
gotopose(x=0.3,y=0.4,z=0.7)
gotopose(x=0.3,y=0.2,z=0.7)

