#!/usr/bin/env python

import rospy
import moveit_commander
import geometry_msgs.msg
import tf.transformations  # Import statement for tf

def go_to_pose(arm, position, orientation):
    pose_target = geometry_msgs.msg.Pose()
    pose_target.position = position
    pose_target.orientation = orientation
    arm.set_pose_target(pose_target)
    arm.go(wait=True)
    arm.clear_pose_targets()

def write_z():
    # Initialize the ROS node
    rospy.init_node('write_z', anonymous=True)
    
    # Initialize the MoveIt! commander for the UR5e arm
    arm = moveit_commander.MoveGroupCommander("manipulator")
    
    # Set the starting position
    arm.set_named_target("home")
    arm.go()
    
    # Quaternion for 180-degree rotation around X-axis
    orientation = geometry_msgs.msg.Quaternion(*tf.transformations.quaternion_from_euler(3.14159, 0, 0))
    
    # Define the points for the letter 'Z'
    points = [
        geometry_msgs.msg.Point(0.3, 0.2, 0.3),  # Top left point of 'Z'
        geometry_msgs.msg.Point(0.5, 0.2, 0.3),  # Top right point of 'Z'
        geometry_msgs.msg.Point(0.3, 0.0, 0.3),  # Bottom left point of 'Z'
        geometry_msgs.msg.Point(0.5, 0.0, 0.3)   # Bottom right point of 'Z'
    ]
    
    for point in points:
        go_to_pose(arm, point, orientation)
    
    # Move back to the home position
    arm.set_named_target("home")
    arm.go()

if __name__ == '__main__':
    try:
        write_z()
    except rospy.ROSInterruptException:
        pass
