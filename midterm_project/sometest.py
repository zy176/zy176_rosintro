#!/usr/bin/env python

import rospy
import moveit_commander
import geometry_msgs.msg
import tf.transformations


def write_z():
    # Initialize the ROS node
    rospy.init_node('write_z', anonymous=True)
    
    # Initialize the MoveIt! commander for the UR5e arm
    arm = moveit_commander.MoveGroupCommander("manipulator")
    
    # Set the starting position
    arm.set_named_target("home")
    arm.go()
    
    # Define the points for the letter 'Z'
    waypoints = []
    
    # Quaternion for 180-degree rotation around X-axis
    orientation = geometry_msgs.msg.Quaternion(*tf.transformations.quaternion_from_euler(3.14159, 0, 0))
    
    # Top left point of 'Z'
    waypoints.append(geometry_msgs.msg.Pose(
        position=geometry_msgs.msg.Point(0.3, 0.2, 0.3),
        orientation=orientation
    ))
    
    # Top right point of 'Z'
    waypoints.append(geometry_msgs.msg.Pose(
        position=geometry_msgs.msg.Point(0.5, 0.2, 0.3),
        orientation=orientation
    ))
    
    # Bottom left point of 'Z'
    waypoints.append(geometry_msgs.msg.Pose(
        position=geometry_msgs.msg.Point(0.3, 0.0, 0.3),
        orientation=orientation
    ))
    
    # Bottom right point of 'Z'
    waypoints.append(geometry_msgs.msg.Pose(
        position=geometry_msgs.msg.Point(0.5, 0.0, 0.3),
        orientation=orientation
    ))
    
    # Plan and execute the trajectory
    (plan, fraction) = arm.compute_cartesian_path(waypoints, 0.01, 0.0)
    arm.execute(plan, wait=True)
    
    # Move back to the home position
    arm.set_named_target("home")
    arm.go()

if __name__ == '__main__':
    try:
        write_z()
    except rospy.ROSInterruptException:
        pass
