import sys
import rospy
import moveit_commander
import numpy as np
import geometry_msgs.msg
from geometry_msgs.msg import Pose 
import time 
import tf.transformations  # Import statement for tf



moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('hw6', anonymous=True)
robot = moveit_commander.RobotCommander()
group = moveit_commander.MoveGroupCommander("manipulator")

#assign joint angles
joint_goal = group.get_current_joint_values()
joint_goal[0] = 0
joint_goal[1] = -np.pi / 4
joint_goal[2] = 0
joint_goal[3] = -np.pi / 2
joint_goal[4] = 0
# 30 degree trun
joint_goal[5] = np.pi / 3  
pose_target = Pose()
orientation = geometry_msgs.msg.Quaternion(*tf.transformations.quaternion_from_euler(3.14159, 0, 0))



def gotopose_init():
	pose_target.position.x = 0.3
	pose_target.position.y = 0.3
	pose_target.position.z = 0.8
	pose_target.orientation = orientation
	group.set_pose_target(pose_target)
	success = group.go(wait=True)
	group.stop()
	group.clear_pose_targets()
	
def gotopose(x, y, z):
	pose_target.position.x = x
	pose_target.position.y = y
	pose_target.position.z = z
	pose_target.orientation = orientation
	group.set_pose_target(pose_target)
	success = group.go(wait=True)
	group.stop()
	group.clear_pose_targets()

print('go to initial position')
gotopose_init()