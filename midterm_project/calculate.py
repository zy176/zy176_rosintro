import sys
import rospy
import moveit_commander
import numpy as np
from geometry_msgs.msg import Pose
import geometry_msgs.msg
import time 
import tf.transformations  # Import statement for tf
from tf.transformations import quaternion_matrix
import moveit_commander

moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('hw6', anonymous=True)
robot = moveit_commander.RobotCommander()
group = moveit_commander.MoveGroupCommander("manipulator")


#assign joint angles
joint_goal = group.get_current_joint_values()
joint_goal[0] = 0
joint_goal[1] = -np.pi / 4
joint_goal[2] = 0
joint_goal[3] = -np.pi / 2
joint_goal[4] = 0
# 30 degree trun
joint_goal[5] = np.pi / 3  
pose_target = Pose()
# Quaternion for 180-degree rotation around X-axis

orientation = geometry_msgs.msg.Quaternion(*tf.transformations.quaternion_from_euler(np.pi, 0, 0))



# function to make the robot go to initial position
def gotopose_init():
	pose_target.position.x = 0.3
	pose_target.position.y = 0.3
	pose_target.position.z = 0.8
	pose_target.orientation = orientation

	group.set_pose_target(pose_target)
	success = group.go(wait=True)
	group.stop()
	group.clear_pose_targets()

# function to make robot go to specific position
def gotopose(x, y, z):
	pose_target.position.x = x
	pose_target.position.y = y
	pose_target.position.z = z
	# pose_target.orientation = orientation
	group.set_pose_target(pose_target)
	success = group.go(wait=True)
	group.stop()
	group.clear_pose_targets()
	
def calculate_transformation_matrix(current_pose):
    position = current_pose.pose.position
    orientation = current_pose.pose.orientation
    # Convert quaternion to rotation matrix
    R = quaternion_matrix([orientation.x, orientation.y, orientation.z, orientation.w])[:3, :3]
    # Create the transformation matrix A
    p = np.array([position.x, position.y, position.z])
    A = np.block([[R, p.reshape(3, 1)], [0, 0, 0, 1]])
    A_formatted = np.vectorize('{:.3g}'.format)(A)
    rospy.loginfo("Transformation Matrix A: \n" + str(A_formatted))
    return A_formatted

def getJacobian(group):
	print("Getting jacobian matrix")

	# Get the current joint values of the group
	current_joint_values = group.get_current_joint_values()

	# Get the Jacobian matrix at the current joint values
	jacobian_matrix = group.get_jacobian_matrix(current_joint_values)

	# Convert the Jacobian matrix to a NumPy array for further processing
	jacobian_np = np.array(jacobian_matrix)
	print(np.vectorize('{:.3g}'.format)(jacobian_np))

print('go to initial position')
gotopose_init()
current_pose = group.get_current_pose()
rospy.loginfo("Current Pose for initial position: \n" + str(current_pose))
calculate_transformation_matrix(current_pose=current_pose)
getJacobian(group=group)
time.sleep(1)

print('draw letter Z')
gotopose(x=0.2,y=0.4,z=0.7)
gotopose(x=0.4,y=0.4,z=0.7)
gotopose(x=0.2,y=0.2,z=0.7)
gotopose(x=0.4,y=0.2,z=0.7)
current_pose = group.get_current_pose()
rospy.loginfo("Current Pose for end of X: \n" + str(current_pose))
calculate_transformation_matrix(current_pose=current_pose)
getJacobian(group=group)

time.sleep(1)

print('go to initial position')
gotopose_init()
time.sleep(1)
print('draw letter Y')
gotopose(x=0.2,y=0.4,z=0.7)
gotopose(x=0.3,y=0.3,z=0.7)
gotopose(x=0.4,y=0.4,z=0.7)
gotopose(x=0.3,y=0.3,z=0.7)
gotopose(x=0.3,y=0.2,z=0.7)
current_pose = group.get_current_pose()
rospy.loginfo("Current Pose for end of Y: \n" + str(current_pose))
calculate_transformation_matrix(current_pose=current_pose)
getJacobian(group=group)


print('go to initial position')
gotopose_init()
time.sleep(1)
print('draw letter T')
gotopose(x=0.2,y=0.4,z=0.7)
gotopose(x=0.4,y=0.4,z=0.7)
gotopose(x=0.3,y=0.4,z=0.7)
gotopose(x=0.3,y=0.2,z=0.7)

